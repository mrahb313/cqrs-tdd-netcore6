using MediatR;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using TodoProject.Command;
using TodoProject.Controllers;
using TodoProject.Interface;
using TodoProject.Model;
using TodoProject.Query;
using Xunit;

namespace TodoTest
{
    public class TodoTest
    {
        private readonly Mock<IRepository> _repository = new();
        private readonly Mock<IMediator> _mediator = new();

        [Fact]
        public async Task should_create()
        {
            bool expected = true;

            Todo todoModel = new()
            {
                id = It.IsAny<int>(),
                name = It.IsAny<string>(),
                description = It.IsAny<string>(),
                isDone = It.IsAny<bool>()
            };

            var query = new CreateTodo.Command(todoModel);
            var handler = new CreateTodo.Handler(_repository.Object);
            var response = new CreateTodo.Response(expected);

            _repository.Setup(repo => repo.Create(todoModel))
                .ReturnsAsync(expected);

            _mediator
            .Setup(m => m.Send(It.IsAny<CreateTodo.Command>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(response);

            var controller = new TodoController(_mediator.Object);
            var result = await controller.CreateTodo(todoModel);

            Assert.Equal(expected, result.Value);
        }

        [Fact]
        public async Task should_fail_create_by_return_false()
        {
            bool expected = false;

            Todo todoModel = new()
            {
                id = It.IsAny<int>(),
                name = It.IsAny<string>(),
                description = It.IsAny<string>(),
                isDone = It.IsAny<bool>()
            };

            var query = new CreateTodo.Command(todoModel);
            var handler = new CreateTodo.Handler(_repository.Object);
            var response = new CreateTodo.Response(expected);

            _repository.Setup(repo => repo.Create(todoModel))
                .ReturnsAsync(expected);

            _mediator
            .Setup(m => m.Send(It.IsAny<CreateTodo.Command>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(response);

            var controller = new TodoController(_mediator.Object);
            var result = await controller.CreateTodo(todoModel);

            Assert.Equal(expected, result.Value);
        }

        [Fact]
        public async Task should_update()
        {
            Todo expected = null;

            Todo todoModel = new()
            {
                id = It.IsAny<int>(),
                name = It.IsAny<string>(),
                description = It.IsAny<string>(),
                isDone = It.IsAny<bool>()
            };

            expected = todoModel;

            var query = new UpdateTodo.Command(todoModel);
            var handler = new UpdateTodo.Handler(_repository.Object);
            var response = new UpdateTodo.Response(expected);

            _repository.Setup(repo => repo.Update(todoModel))
                .ReturnsAsync(expected);

            _mediator
            .Setup(m => m.Send(It.IsAny<UpdateTodo.Command>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(response);

            var controller = new TodoController(_mediator.Object);
            var result = await controller.UpdateTodo(todoModel);

            Assert.NotNull(result.Value);
            Assert.Equal(expected, result.Value);
        }

        [Fact]
        public async Task should_fail_update_by_notFound()
        {
            Todo expected = null;

            Todo todoModel = new()
            {
                id = It.IsAny<int>(),
                name = It.IsAny<string>(),
                description = It.IsAny<string>(),
                isDone = It.IsAny<bool>()
            };

            var query = new UpdateTodo.Command(todoModel);
            var handler = new UpdateTodo.Handler(_repository.Object);
            var response = new UpdateTodo.Response(expected);

            _repository.Setup(repo => repo.Update(todoModel))
                .ReturnsAsync(expected);

            _mediator
            .Setup(m => m.Send(It.IsAny<UpdateTodo.Command>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(response);

            var controller = new TodoController(_mediator.Object);
            var result = await controller.UpdateTodo(todoModel);

            Assert.Null(result.Value);
            Assert.IsType<NotFoundResult>(result.Result);
        }

        [Fact]
        public async Task should_getById()
        {
            Todo expected = null;

            Todo todoModel = new()
            {
                id = It.IsAny<int>(),
                name = It.IsAny<string>(),
                description = It.IsAny<string>(),
                isDone = It.IsAny<bool>()
            };

            expected = todoModel;

            var query = new GetTodoById.Query(It.IsAny<int>());
            var handler = new GetTodoById.Handler(_repository.Object);
            var response = new GetTodoById.Response(expected);

            _repository.Setup(repo => repo.GetById(It.IsAny<int>()))
                .ReturnsAsync(expected);

            _mediator
            .Setup(m => m.Send(It.IsAny<GetTodoById.Query>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(response);

            var controller = new TodoController(_mediator.Object);
            var result = await controller.GetById(It.IsAny<int>());

            Assert.NotNull(result.Value);
            Assert.Equal(expected, result.Value);
        }

        [Fact]
        public async Task should_fail_getById_by_notFound()
        {
            Todo expected = null;

            Todo todoModel = new()
            {
                id = It.IsAny<int>(),
                name = It.IsAny<string>(),
                description = It.IsAny<string>(),
                isDone = It.IsAny<bool>()
            };

            var query = new GetTodoById.Query(It.IsAny<int>());
            var handler = new GetTodoById.Handler(_repository.Object);
            var response = new GetTodoById.Response(expected);

            _repository.Setup(repo => repo.GetById(It.IsAny<int>()))
                .ReturnsAsync(expected);

            _mediator
            .Setup(m => m.Send(It.IsAny<GetTodoById.Query>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(response);

            var controller = new TodoController(_mediator.Object);
            var result = await controller.GetById(It.IsAny<int>());

            Assert.Null(result.Value);
            Assert.IsType<NotFoundResult>(result.Result);
        }

        [Fact]
        public async Task should_getAll()
        {
            List<Todo> expected = new()
            {
                new Todo { id = It.IsAny<int>(), name = It.IsAny<string>(), description = It.IsAny<string>(), isDone = It.IsAny<bool>() },
                new Todo { id = It.IsAny<int>(), name = It.IsAny<string>(), description = It.IsAny<string>(), isDone = It.IsAny<bool>() },
                new Todo { id = It.IsAny<int>(), name = It.IsAny<string>(), description = It.IsAny<string>(), isDone = It.IsAny<bool>() },
                new Todo { id = It.IsAny<int>(), name = It.IsAny<string>(), description = It.IsAny<string>(), isDone = It.IsAny<bool>() },
            };

            var query = new GetAllTodo.Query();
            var handler = new GetAllTodo.Handler(_repository.Object);
            var response = new GetAllTodo.Response(expected);

            _repository.Setup(repo => repo.GetAll())
                .ReturnsAsync(expected);

            _mediator
            .Setup(m => m.Send(It.IsAny<GetAllTodo.Query>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(response);

            var controller = new TodoController(_mediator.Object);
            var result = await controller.GetAll();

            Assert.NotEmpty(result);
        }

        [Fact]
        public async Task should_delete()
        {
            bool expected = true;

            var query = new DeleteTodo.Command(It.IsAny<int>());
            var handler = new DeleteTodo.Handler(_repository.Object);
            var response = new DeleteTodo.Response(expected);

            _repository.Setup(repo => repo.Delete(It.IsAny<int>()))
                .ReturnsAsync(expected);

            _mediator
            .Setup(m => m.Send(It.IsAny<DeleteTodo.Command>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(response);

            var controller = new TodoController(_mediator.Object);
            var result = await controller.DeleteById(It.IsAny<int>());

            Assert.Equal(expected, result.Value);
        }

        [Fact]
        public async Task should_fail_delete_by_notFound()
        {
            bool expected = false;

            var query = new DeleteTodo.Command(It.IsAny<int>());
            var handler = new DeleteTodo.Handler(_repository.Object);
            var response = new DeleteTodo.Response(expected);

            _repository.Setup(repo => repo.Delete(It.IsAny<int>()))
                .ReturnsAsync(expected);

            _mediator
            .Setup(m => m.Send(It.IsAny<DeleteTodo.Command>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(response);

            var controller = new TodoController(_mediator.Object);
            var result = await controller.DeleteById(It.IsAny<int>());

            Assert.Equal(expected, result.Value);
        }


    }
}