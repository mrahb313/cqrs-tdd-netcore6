﻿using MediatR;
using TodoProject.Interface;
using TodoProject.Model;

namespace TodoProject.Query
{
    public class GetAllTodo
    {
        public record Query() : IRequest<Response>;

        public class Handler : IRequestHandler<Query, Response>
        {
            private readonly IRepository _repo;
            public Handler(IRepository repo)
            {
                _repo = repo;
            }
            public async Task<Response> Handle(Query request, CancellationToken cancellationToken)
            {
                var result = await _repo.GetAll();
                return new Response(result);
            }
        }

        public record Response(List<Todo> todos);
    }
}
