﻿using MediatR;
using TodoProject.Interface;
using TodoProject.Model;

namespace TodoProject.Query
{
    public class GetTodoById
    {
        public record Query(int id) : IRequest<Response>;

        public class Handler : IRequestHandler<Query, Response>
        {
            private readonly IRepository _repo;
            public Handler(IRepository repo)
            {
                _repo = repo;
            }
            public async Task<Response> Handle(Query request, CancellationToken cancellationToken)
            {
                var getTodo = await _repo.GetById(request.id);
                return getTodo == null ? null : new Response(getTodo);
            }
        }

        public record Response(Todo todo);

    }
}
