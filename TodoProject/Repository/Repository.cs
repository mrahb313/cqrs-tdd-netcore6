﻿using TodoProject.Interface;
using TodoProject.Model;

namespace TodoProject.Repository
{
    public class Repository : IRepository
    {
        List<Todo> _todoList;
        public Repository()
        {
            _todoList = new()
            {
                new Todo { id = 1, name = "test1", description = "des1", isDone = false },
                new Todo { id = 2, name = "test2", description = "des2", isDone = true },
                new Todo { id = 3, name = "test3", description = "des3", isDone = true },
                new Todo { id = 4, name = "test4", description = "des4", isDone = false },
            };
        }
        public async Task<bool> Create(Todo model)
        {
            try
            {
                int id;
                var getLatestTodo = _todoList.LastOrDefault();

                if (getLatestTodo == null)
                    id = 1;
                else
                    id = getLatestTodo.id;

                model.id = ++id;
                _todoList.Add(model);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public async Task<bool> Delete(int id)
        {
            var getTodo = _todoList.FirstOrDefault(c => c.id == id);

            if (getTodo == null)
                return false;
            else
            {
                _todoList.Remove(getTodo);
                return true;
            }

        }

        public async Task<List<Todo>> GetAll()
        {
            return _todoList;
        }

        public async Task<Todo> GetById(int id)
        {
            var getTodo = _todoList.FirstOrDefault(c => c.id == id);
            return getTodo == null ? null : getTodo;
        }

        public async Task<Todo> Update(Todo model)
        {
            int id;
            var getTodo = _todoList.FirstOrDefault(c => c.id == model.id);

            if (getTodo == null)
                return null;
            else
            {
                id = getTodo.id;
                _todoList.Remove(getTodo);

                var updateModel = new Todo { id = id, name = model.name, description = model.description, isDone = model.isDone };
                _todoList.Add(updateModel);

                return updateModel;
            }
        }
    }
}
