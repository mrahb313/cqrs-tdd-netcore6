﻿using TodoProject.Model;

namespace TodoProject.Interface
{
    public interface IRepository
    {
        Task<bool> Create(Todo model);
        Task<Todo> GetById(int id);
        Task<Todo> Update(Todo model);
        Task<bool> Delete(int id);
        Task<List<Todo>> GetAll();
    }
}
