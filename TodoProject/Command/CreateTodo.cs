﻿using MediatR;
using TodoProject.Interface;
using TodoProject.Model;

namespace TodoProject.Command
{
    public class CreateTodo
    {
        public record Command(Todo todo) : IRequest<Response>;


        public class Handler : IRequestHandler<Command, Response>
        {
            private readonly IRepository _repo;

            public Handler(IRepository repo)
            {
                _repo = repo;
            }
            public async Task<Response> Handle(Command request, CancellationToken cancellationToken)
            {
                var result = await _repo.Create(request.todo);
                return new Response(result);
            }
        }

        public record Response(bool isAdded);

    }
}
