﻿using MediatR;
using TodoProject.Interface;
using TodoProject.Model;

namespace TodoProject.Command
{
    public class UpdateTodo
    {
        public record Command(Todo todo) : IRequest<Response>;

        public class Handler : IRequestHandler<Command, Response>
        {
            private readonly IRepository _repo;

            public Handler(IRepository repo)
            {
                _repo = repo;
            }
            public async Task<Response> Handle(Command request, CancellationToken cancellationToken)
            {
                var result = await _repo.Update(request.todo);
                return new Response(result);
            }
        }

        public record Response(Todo todo);
    }
}
