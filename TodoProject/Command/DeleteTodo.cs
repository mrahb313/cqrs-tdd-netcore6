﻿using MediatR;
using TodoProject.Interface;

namespace TodoProject.Command
{
    public class DeleteTodo
    {
        public record Command(int id) : IRequest<Response>;

        public class Handler : IRequestHandler<Command, Response>
        {
            private readonly IRepository _repo;

            public Handler(IRepository repo)
            {
                _repo = repo;
            }
            public async Task<Response> Handle(Command request, CancellationToken cancellationToken)
            {
                var result = await _repo.Delete(request.id);
                return new Response(result);
            }
        }

        public record Response(bool isDeleted);
    }
}
