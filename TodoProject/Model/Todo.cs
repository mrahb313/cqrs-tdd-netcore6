﻿namespace TodoProject.Model
{
    public class Todo
    {
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public bool isDone { get; set; }
    }
}
