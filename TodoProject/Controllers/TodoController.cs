﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TodoProject.Command;
using TodoProject.Model;
using TodoProject.Query;

namespace TodoProject.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TodoController : ControllerBase
    {
        private readonly IMediator _mediator;

        public TodoController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost]
        public async Task<ActionResult<bool>> CreateTodo([FromBody] Todo model)
        {
            var result = await _mediator.Send(new CreateTodo.Command(model));
            return result.isAdded;
        }

        [HttpPut]
        public async Task<ActionResult<Todo>> UpdateTodo([FromBody] Todo model)
        {
            var result = await _mediator.Send(new UpdateTodo.Command(model));

            if (result.todo == null)
                return NotFound();

            return result.todo;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Todo>> GetById([FromRoute] int id)
        {
            var result = await _mediator.Send(new GetTodoById.Query(id));

            if (result.todo == null)
                return NotFound();

            return result.todo;
        }

        [HttpGet]
        public async Task<List<Todo>> GetAll()
        {
            var result = await _mediator.Send(new GetAllTodo.Query());
            return result.todos;
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<bool>> DeleteById([FromRoute] int id)
        {
            var result = await _mediator.Send(new DeleteTodo.Command(id));
            return result.isDeleted; ;
        }
    }
}
